package client;

import javafx.application.Application;
import javafx.stage.Stage;

public class Main extends Application{
	private Controller controller;
	private View view;
	private Model model;

	public static void main(String[] args) {
		launch(args);
	}
	
	public void start (Stage stage) throws Exception{
		model = new Model();
		view = new View(stage, model);
		controller = new Controller(model, view);
		view.start();
		
	}

}
