package client;

import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class View {
	private Model model;
	protected Stage stage;
	
	protected Label ipAddressLbl, portLbl, nameLbl, emailLbl, passwordLbl, taskLbl, priorityLbl, descriptionLbl;
	protected Label insertTokenHere;
	protected Label placeHolder, placeHolder1, placeHolder2, placeHolder3, placeHolder4, placeHolder5, placeHolder6, placeHolder7;
	protected TextField ipTF, portTF, commandTF, emailTF, passwordTF, taskTF, descriptionTF, tokenTF, taskIDTF;
	protected Button connectButton, sendButton, createLoginButton, loginButton, addTaskButton, logoutButton, changePWButton;
	protected Button listToDosButton, getTodoButton, deleteTodoButton, pingButton, backButton, nextButton;
	protected TextArea textArea;
	ChoiceBox prioChoice;
	
	
	
	public View(Stage stage, Model model) {
		this.stage = stage;
		this.model = model;
		
		VBox root = new VBox();
		this.textArea = new TextArea();
		root.getStyleClass().add("root");
		
		GridPane gridUP = new GridPane();
		this.ipAddressLbl = new Label ("IP Address");
		this.ipTF = new TextField();
		this.portLbl = new Label ("Port");
		this.portTF = new TextField();
		
		this.emailLbl = new Label("e-Mail");
		this.emailTF = new TextField();
		this.passwordLbl = new Label("Password");
		this.passwordTF = new TextField();
		
		this.loginButton = new Button("Login");
		
		
		this.connectButton = new Button("Connect");
		
		
		this.createLoginButton = new Button("Create Login");
		this.createLoginButton.setDisable(true);
		
		this.changePWButton= new Button("Change Password");
		this.changePWButton.setDisable(true);
		this.logoutButton= new Button("Logout");
		
		this.pingButton = new Button("PING");
		
		this.placeHolder = new Label("          ");
		this.placeHolder1 = new Label();
		this.placeHolder2 = new Label();
		
		gridUP.add(ipAddressLbl, 0, 0);
		gridUP.add(ipTF, 1, 0);
		gridUP.add(placeHolder, 2, 0);
		gridUP.add(portLbl, 3, 0);
		gridUP.add(portTF, 4, 0);
		gridUP.add(connectButton, 5, 0);
		
		gridUP.add(placeHolder1, 0, 1);
		
		gridUP.add(emailLbl, 0, 2);
		gridUP.add(emailTF, 1, 2);
		gridUP.add(passwordLbl, 3, 2);
		gridUP.add(passwordTF, 4, 2);
		gridUP.add(createLoginButton, 5, 2);
		gridUP.add(loginButton, 6, 2);
		gridUP.add(placeHolder2, 0, 3);		
		gridUP.add(changePWButton, 10, 4);
		gridUP.add(logoutButton, 9, 4);
		
		gridUP.add(pingButton, 0, 4);
		
				
		GridPane grid = new GridPane();
		
	
		Label todos = new Label("To Do's");
		Label token = new Label("Token: ");
		this.insertTokenHere = new Label("<- Insert code given at login");
		Label taskID = new Label("Task ID: ");
		Label manageToDo = new Label("Manage your to-do's");
		this.taskLbl = new Label ("Task title");
		this.taskTF = new TextField();
		this.priorityLbl = new Label("Priority");
		this.prioChoice = new ChoiceBox();
		prioChoice.getItems().add("Low");
		prioChoice.getItems().add("Medium");
		prioChoice.getItems().add("High");
		prioChoice.getSelectionModel().select(0);
		this.descriptionLbl = new Label("Description");
		this.descriptionTF = new TextField();
		this.addTaskButton = new Button("Add Task");
		this.addTaskButton.setDisable(true);
		this.tokenTF = new TextField();
		this.listToDosButton = new Button("List to-do's");
		this.getTodoButton = new Button("Get to-do");
		this.getTodoButton.setDisable(true);
		this.deleteTodoButton= new Button ("Delete to-do");
		this.deleteTodoButton.setDisable(true);
		this.taskIDTF= new TextField();
		this.placeHolder3 = new Label();
		this.placeHolder4 = new Label();
		this.placeHolder5 = new Label();
		this.placeHolder6 = new Label("          ");
		this.placeHolder7 = new Label();
		
		grid.add(todos, 0, 2);
		grid.add(token, 0, 3);
		grid.add(tokenTF, 1, 3);
		grid.add(insertTokenHere, 2, 3, 3, 1);
		grid.add(placeHolder3, 0, 4);
		grid.add(manageToDo, 0, 5);
		grid.add(taskLbl, 0, 6);
		grid.add(taskTF, 1, 6);
		grid.add(placeHolder5, 2, 6);
		grid.add(priorityLbl, 3, 6);
		grid.add(prioChoice, 4, 6);
		grid.add(placeHolder6, 5, 6);
		grid.add(descriptionLbl, 6, 6);
		grid.add(descriptionTF, 7, 6);
		grid.add(addTaskButton, 8, 6);
		grid.add(placeHolder4, 0, 7);
		grid.add(listToDosButton, 0, 8);
		grid.add(placeHolder7, 0, 9);
		grid.add(taskID, 0, 10);
		grid.add(taskIDTF, 1, 10);
		grid.add(getTodoButton, 2, 10);
		grid.add(deleteTodoButton, 3, 10);
		

	
		
		
		// Prevent labels and button from shrinking below their preferred size
		ipAddressLbl.setMinSize(Button.USE_PREF_SIZE, Button.USE_PREF_SIZE);
		portLbl.setMinSize(Button.USE_PREF_SIZE, Button.USE_PREF_SIZE);
		//nameLbl.setMinSize(Button.USE_PREF_SIZE, Button.USE_PREF_SIZE);
		connectButton.setMinSize(Button.USE_PREF_SIZE, Button.USE_PREF_SIZE);
		//sendButton.setMinSize(Button.USE_PREF_SIZE, Button.USE_PREF_SIZE);
		
		// Set sizes for top TextFields
		ipTF.setMinWidth(150); ipTF.setPrefWidth(150);
		portTF.setMinWidth(60); portTF.setPrefWidth(60);
		//nameTF.setMinWidth(150); nameTF.setPrefWidth(150);
		
		
		root.getChildren().addAll(gridUP, textArea, grid);
		
		Scene scene = new Scene(root);
		scene.getStylesheets().add(getClass().getResource("client.css").toExternalForm());
		
		stage.setScene(scene);
		stage.setTitle("Client");
			
	}

	public void start() {
		stage.show();
		
		// Prevent resizing below initial size
		stage.setMinWidth(stage.getWidth());
		stage.setMinHeight(stage.getHeight());
		
	}

}
