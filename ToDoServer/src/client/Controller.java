package client;

import java.util.ArrayList;

import javafx.beans.binding.Bindings;
import server.ToDo;

public class Controller {
	private Model model;
	private View view;
	
	private boolean taskValid, descriptionValid, emailValid, passwordValid, tokenValid, taskIDValid;
	

	public Controller(Model model, View view) {
		this.model = model;
		this.view = view;
		
		view.connectButton.setOnAction(event -> {
			view.connectButton.setDisable(true);
			String ipAddress = view.ipTF.getText();
			int port = Integer.parseInt(view.portTF.getText());
			model.connect(ipAddress, port);
		});
		
		view.createLoginButton.setOnAction(event -> {
			model.sendCreateLoginMessage(view.emailTF.getText(), view.passwordTF.getText());
		});
		
		view.stage.setOnCloseRequest(event -> model.disconnect() );
		
		
		view.loginButton.setOnAction(event -> {
			model.sendLoginMessage(view.emailTF.getText(), view.passwordTF.getText());	
		});
		
		view.changePWButton.setOnAction(event -> model.sendChangePWMessage(view.tokenTF.getText(), view.passwordTF.getText()));
		view.logoutButton.setOnAction(event-> {
			model.sendLogoutMessage(view.tokenTF.getText());
			view.loginButton.setDisable(false);
			view.tokenTF.clear();

			
		});
		view.addTaskButton.setOnAction(event -> {
			model.sendCreateToDoMsg(view.tokenTF.getText(), view.taskTF.getText(),(view.prioChoice.getValue().toString()), view.descriptionTF.getText());
			view.taskTF.clear();
			view.descriptionTF.clear();
			view.prioChoice.getSelectionModel().select(0);
			
		});
		view.getTodoButton.setOnAction(event-> {
			model.sendGetToDoMsg(view.tokenTF.getText(), view.taskIDTF.getText());
		
		});
		view.deleteTodoButton.setOnAction(event-> {
			model.sendDeleteToDoMsg(view.tokenTF.getText(), view.taskIDTF.getText());
			view.taskIDTF.clear();
		});
		view.listToDosButton.setOnAction(event-> model.sendListToDosMsg(view.tokenTF.getText()));
		view.pingButton.setOnAction(event -> model.sendPingMsg(view.tokenTF.getText(), view.tokenTF.getText().isEmpty()));
		
		//enable / disable buttons
		view.logoutButton.disableProperty().bind(Bindings.isEmpty(view.tokenTF.textProperty()));
		view.listToDosButton.disableProperty().bind(Bindings.isEmpty(view.tokenTF.textProperty()));
		
		view.emailTF.textProperty().addListener((observable, oldValue, newValue) -> {
			validateEmailAddress(newValue);
		});
		view.passwordTF.textProperty().addListener((observable, oldValue, newValue) -> {
			validatePassword(newValue);
		});
		
		
		view.taskTF.textProperty().addListener((observable, oldValue, newValue) -> {
			validateTask(newValue);
		});
		view.descriptionTF.textProperty().addListener((observable, oldValue, newValue) -> {
			validateDescription(newValue);
		});
		
		view.tokenTF.textProperty().addListener((observable, oldValue, newValue) -> {
			validateToken(newValue);
		});
		view.taskIDTF.textProperty().addListener((observable, oldValue, newValue) -> {
			validateTaskID(newValue);
		});
		
		
		model.newestMessage.addListener( (o, oldValue, newValue) ->  {
		if( !newValue.isEmpty()) //Ignore empty messages
			view.textArea.appendText(newValue + "\n");
		} );
		
	
	
	}
	


	private void validateTaskID(String newValue) {
		boolean valid = false;
		if (newValue.length() >= 1) {
			valid = true;
		}taskIDValid = valid;
		enableGetDeleteButtons();
		
	}

	private void enableGetDeleteButtons() {
		boolean valid = tokenValid & taskIDValid;
		view.getTodoButton.setDisable(!valid);
		view.deleteTodoButton.setDisable(!valid);
		
	}

	private void validateToken(String newValue) {
		boolean valid = false; 
		if (newValue.length()>=1) {
			valid = true;
		}tokenValid = valid;
		enableChangePWButton();
		enableAddTaskButton();
		enableGetDeleteButtons();
	}

	private void validateDescription(String newValue) {
		if (newValue.length() > 255) {
			view.addTaskButton.setDisable(true);
		}		
	}

	private void validateTask(String newValue) {
		boolean valid = false;
		
		if (newValue.length() >=3 && newValue.length() <=20) {
			valid = true;
		}
		taskValid = valid;
		enableAddTaskButton();
		
		
	}

	private void enableAddTaskButton() {
		boolean valid = taskValid & tokenValid;
		view.addTaskButton.setDisable(!valid);
		
	}

	private void validatePassword(String newValue) {
		boolean valid = false;
		if(newValue.length() >= 3 && newValue.length() <=20) {
			valid = true;
		}
		passwordValid = valid;
		enableLoginButtons();
		enableChangePWButton();

		
	}

	private void enableChangePWButton() {
		boolean valid = passwordValid & tokenValid;
		view.changePWButton.setDisable(!valid);
		
	}

	private void validateEmailAddress(String newValue) {
		boolean valid = false; 
		
		String[] addressParts = newValue.split("@");
		if(addressParts.length == 2 && !addressParts[0].isEmpty() && !addressParts[1].isEmpty()) {
			if(addressParts[1].charAt(addressParts[1].length() -1) != '.') {
				String[] domainParts = addressParts[1].split("\\.");
					if (domainParts.length >=2 ) {
						valid = true;
						for (String s : domainParts) {
							if(s.length() < 2) valid = false;
						}
					}
				}
			}emailValid = valid;
			enableLoginButtons();
		
		}

	

	private void enableLoginButtons() {
		boolean valid = emailValid & passwordValid;
		view.loginButton.setDisable(!valid);
		view.createLoginButton.setDisable(!valid);
		
	}
	
	}

