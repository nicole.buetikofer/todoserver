package client;

import java.io.IOException;
import java.net.Socket;
import java.util.ArrayList;
import java.util.logging.Logger;

import commons.ChangePasswordMsg;
import commons.ChatMsg;
import commons.CreateLoginMsg;
import commons.CreateToDoMsg;
import commons.DeleteToDoMsg;
import commons.GetToDoMsg;
import commons.JoinMsg;
import commons.ListToDosMsg;
import commons.LoginMsg;
import commons.LogoutMsg;
import commons.Message;
import commons.MessageType;
import commons.PingMsg;
import commons.ResultLoginMsg;
import commons.ResultMsg;
import javafx.beans.binding.StringExpression;
import javafx.beans.property.SimpleStringProperty;
import server.Priority;
import server.ToDo;

public class Model {
protected SimpleStringProperty newestMessage = new SimpleStringProperty();
protected ArrayList<ToDo> toDos = new ArrayList<ToDo>();
	
	private Logger logger = Logger.getLogger("");
	private Socket socket;
	private String name;
	
	public void connect(String ipAddress, int Port) {
		logger.info("Connect");
		try {
			socket = new Socket (ipAddress, Port);
			
			//Create thread to read incoming messages
			Runnable r = new Runnable() {
				@Override
				public void run() {
					while (true) {
						
									
						Message msg = Message.receive(socket);
					
						String message = msg.toString();
					
							newestMessage.set(""); //erase previous message
							newestMessage.set(message);
		
					}
				}
			};
			Thread t = new Thread(r);
			t.start();

			
		}catch (Exception e) {
			logger.warning(e.toString());
		}
	}
	
	public void disconnect() {
		logger.info("Disconnect");
		if (socket != null)
			try {
				socket.close();
			}catch (IOException e) {
			}
	}
	
	
	public String receiveMessage() {
		logger.info("Receive message");
		return newestMessage.get();
		
	}

	public void sendCreateLoginMessage(String email, String password) {
		logger.info("Send message");
		Message msg = new CreateLoginMsg(email, password);
		msg.send(socket);
		
		newestMessage.set(""); //erase previous message
		newestMessage.set(msg.toString());
		
	}

	public void sendLoginMessage(String email, String password) {
		logger.info("Send message");
		Message msg = new LoginMsg(email, password);
		msg.send(socket);
		newestMessage.set(""); //erase previous message
		newestMessage.set(msg.toString());
	}

	

	public void sendLogoutMessage(String token) {
		logger.info("Send message");
		Message msg = new LogoutMsg(token);
		msg.send(socket);
		newestMessage.set(""); //erase previous message
		newestMessage.set(msg.toString());
		
	}

	public void sendChangePWMessage(String tokenAsString, String newPassword) {
		logger.info("Send message");
		Message msg = new ChangePasswordMsg(tokenAsString, newPassword);
		msg.send(socket);
		newestMessage.set(""); //erase previous message
		newestMessage.set(msg.toString());
	}

	public void sendCreateToDoMsg(String tokenAsString, String task, String prio, String description) {
		logger.info("Send message");
		Priority priority = Priority.valueOf(prio);
		if(description.equals("")) {
			description = "-";
		}

		Message msg = new CreateToDoMsg(tokenAsString, task, priority, description);
		msg.send(socket);
		newestMessage.set(""); //erase previous message
		newestMessage.set(msg.toString());
		
	}

	public void sendGetToDoMsg(String tokenAsString, String taskIDAsString) {
		logger.info("Send message");

		int id = Integer.parseInt(taskIDAsString);
		Message msg = new GetToDoMsg(tokenAsString, id);
		msg.send(socket);
		newestMessage.set(""); //erase previous message
		newestMessage.set(msg.toString());
	}

	public void sendDeleteToDoMsg(String tokenAsString, String taskIDAsString) {
		logger.info("Send message");
		int id = Integer.parseInt(taskIDAsString);
		Message msg = new DeleteToDoMsg(tokenAsString, id);
		msg.send(socket);
		newestMessage.set(""); //erase previous message
		newestMessage.set(msg.toString());
	}

	public void sendListToDosMsg(String tokenAsString) {
		logger.info("Send message");
		Message msg = new ListToDosMsg(tokenAsString);
		msg.send(socket);
		newestMessage.set(""); //erase previous message
		newestMessage.set(msg.toString());
	}

	public void sendPingMsg(String tokenAsString, boolean noToken) {
		Message msg;
		logger.info("Send message");
		if (noToken) {
			msg = new PingMsg();
		}else {
			msg = new PingMsg(tokenAsString);
		}
		msg.send(socket);
		newestMessage.set(""); //erase previous message
		newestMessage.set(msg.toString());
		
	}

	public void createToDo(String taskTitle, String priority, String description, String userName, String id) {
		Priority prio = Priority.valueOf(priority);
		int taskID = Integer.parseInt(id);
		if(description == null) {
			description = "-";
		}
		ToDo todo = new ToDo(taskTitle, prio, description, userName, taskID);
		toDos.add(todo);
		
	}

	

	

	

}
