package server;

public class Account {
	private String userName;
	private String password;
	
	public Account(String userName, String password) {
		this.userName = userName;
		this.password= password;
	}
	
	public String getUserName() {
		return this.userName;
	}
	public void setUserName (String userName) {
		this.userName = userName;
	}
	
	public String getPassword() {
		return this.password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
		
	public boolean verifyPassword(String password) {
		boolean valid = false;
		if (password.length() >= 3 && password.length() <= 20) {
			valid = true;
		}
		return valid;
	}
	
	public boolean equals(Account otherAcc) {
		boolean equals = false;
		if (this.userName.equals(otherAcc.userName)) {
			equals = true;
		}return equals;
	}

}
