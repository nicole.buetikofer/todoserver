package server;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Reader;
import java.io.Writer;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.ListIterator;
import java.util.Random;
import java.util.logging.Logger;

import commons.ChangePasswordMsg;
import commons.ChatMsg;
import commons.CreateLoginMsg;
import commons.CreateToDoMsg;
import commons.DeleteToDoMsg;
import commons.GetToDoMsg;
import commons.ListToDosMsg;
import commons.LoginMsg;
import commons.LogoutMsg;
import commons.Message;
import commons.PingMsg;
import commons.ResultMsg;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class Model {
	protected final ObservableList<Client> clients = FXCollections.observableArrayList();
	protected ArrayList<Account> accounts = new ArrayList<Account>();
	protected ArrayList<Integer> tokens = new ArrayList<Integer>();
	protected ArrayList<ToDo> todos = new ArrayList<ToDo>();
	
	private static String TODOS_FILE = "todos.txt";
	private static String ACCOUNTS_FILE = "accounts.txt";
	private static String SEPARATOR = ";"; // Separator for "split"
	
	Random rand = new Random();
	
	private final Logger logger = Logger.getLogger("");
	private ServerSocket listener;
	private volatile boolean stop = false;
	
	public void startServer(int port) {
		logger.info("Start server");
		try {
			listener = new ServerSocket(port, 10, null);
			Runnable r = new Runnable() {
				@Override
				public void run() {
					while (!stop) {
					try {
						Socket socket = listener.accept();
						Client client = new Client(Model.this, socket);
						clients.add(client);
					}catch (Exception e) {
						logger.info(e.toString());
					}
				}
			}
		};
		Thread t = new Thread(r, "ServerSocket");
		t.start();
	}catch (IOException e) {
		logger.info(e.toString());
		}
	}
	public void stopServer() {
		logger.info("Stop all clients");
		for (Client c : clients) c.stop();
		
		logger.info("Stop server");
		stop = true;
		if(listener != null) {
			try {
				listener.close();
			}catch (IOException e) {
			}
		}
	}


	public void createAccount(String userName, String password) {
				
		Account acc = new Account (userName, password);
		accounts.add(acc);
	}
	public int loginAccount(String userName, String password) {
		int token = 0;
		Account acc = new Account (userName, password);
		for (Account a : accounts) {
			if (a.equals(acc)) {
				//Position im Array vom account wird zurückgegeben
				int pos = accounts.indexOf(a);
				//token wird erstellt
				token = createToken();
				//token wird in einem separaten Array an der gleichen Stelle wie der Account gespeichert
				tokens.add(pos, token);
			}
		}return token;
	}
	public boolean checkLogin(String userName, String password) {
		boolean loginOK = false;
		Account acc = new Account(userName, password);
		for (Account a : accounts) {
			if (a.equals(acc)) {
			loginOK = true;
			}
		}
		return loginOK;
	}
	private int createToken() {
		int token = rand.nextInt(1000000) +1;
		return token;
	}
	
	public boolean validateToken(int token) {
		boolean tokenOK = false;	
		for (int t : tokens) {
			if (token == t) {
				tokenOK = true;
			}
		} return tokenOK;
			
		}
	public void sendResult(Message msg) {
		logger.info("Send message");
		String message = msg.toString();
		String[] messageParts = message.split("|");
		if(messageParts.length > 1) {
			int token = Integer.parseInt(messageParts[1].toString());
			if(validateToken(token)) {
				Message m = new ResultMsg(token);
			}else {
				boolean b = true;
				Message m = new ResultMsg(b);
			}
			
		
				
		}
	}
	
	public void logout(Message msg) {
		// TODO Token becomes invalid
		
	}
	public int createToDo(String taskTitle, Priority priority, String description, int token) {
		String userName = getUserName(token);
		ToDo todo = new ToDo(taskTitle, priority, description, userName);
		todos.add(todo);
		int iD= todo.getTaskID();
		
		return iD;
	}
	private String getUserName(int token) {
		String userName = "";
		int pos = 0;
		for (int t : tokens) {
			if(t == token) {
				pos = tokens.indexOf(t);
			}
		}
		userName = accounts.get(pos).getUserName();
		return userName;
		
	}
	public String getToDoIDs(int token) {
		ArrayList<String> iDs = new ArrayList<String>();
		String userName = getUserName(token);
		String answer = "";
		
		for (ToDo t : todos) {
			if(t.getUserName().equals(userName)) {
				int tID = t.getTaskID();
				String id = Integer.toString(tID);
				iDs.add(id);
			}
		}
		for (String s : iDs) {
			answer += "|" +s;
		}
		return answer;
	}
	
	public String getToDoTitle(int taskID) {
		String taskTitle = "";
		for (ToDo t : todos){
			if (t.getTaskID() == taskID) {
				taskTitle = t.getTaskTitle();
			}
		}
		return taskTitle;
	}
	
	public boolean validateTaskID(int taskID) {
		boolean valid = false;
		for (ToDo t : todos){
			if (t.getTaskID() == taskID) {
				valid = true;
			}
		}
		return valid; 
	}
	public void deleteToDo(int taskID) {
		int i = 0;
		for (ToDo t : todos) {
			if(t.getTaskID() == taskID) {
				i = todos.indexOf(t);
			}
		}
		todos.remove(i);
		
	}
	public void setTokenInvalid(int token) {
		int i = 0;
		for (int t : tokens) {
			if(t == token) {
				i = tokens.indexOf(t);
			}
		}
		tokens.set(i, 0);
	}
	public Priority getToDoPriority(int taskID) {
		Priority prio = Priority.Low;
		for (ToDo t : todos){
			if (t.getTaskID() == taskID) {
				prio = t.getPriority();
			}
		}
		return prio;
	}
	public String getToToDescription(int taskID) {
		String description = " ";
		for (ToDo t : todos) {
			if (t.getTaskID() == taskID) {
				description = t.getTaskDescription();
			}
		}
		return description;
	}
	public void setNewPassword(int token, String newPassword) {
		int pos = 0;
		for (int t : tokens) {
			if (t == token) {
				pos = tokens.indexOf(t);
			}
		}Account a = accounts.get(pos);
		a.setPassword(newPassword);
		
	}
	
	public boolean checkIfPWisNew(int token, String newPassword) {
		boolean valid = false;
		int pos = 0;
		for (int t : tokens) {
			if (t == token) {
				pos = tokens.indexOf(t);
			}
		}Account a = accounts.get(pos);
		if(!accounts.get(pos).getPassword().equals(newPassword)) {
			valid = true;
		}
		return valid;
	}
	public void saveTodos() {
		File todoFile = new File(TODOS_FILE);
		try (Writer out = new FileWriter(todoFile)){
			for (ToDo t : todos) {
				String line = writeTask(t);
				out.write(line);
			}
		}catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	private String writeTask(ToDo t) {
		String line = t.getTaskTitle() + SEPARATOR + t.getPriority() + SEPARATOR + t.getTaskDescription() + SEPARATOR + t.getUserName() + "\n";
		return line;
	}
	public void saveAccounts() {
		File accountsFile = new File(ACCOUNTS_FILE);
		try(Writer out = new FileWriter(accountsFile)){
			for (Account a : accounts) {
				String line = writeAccount(a);
				out.write(line);
			}
		}catch(Exception e) {
			e.printStackTrace();
		}
		
	}
	private String writeAccount(Account a) {
		String line = a.getUserName() + SEPARATOR + a.getPassword() + SEPARATOR + "\n";
		return line;
	}

	public void loadTodos() {
		File todoFile = new File(TODOS_FILE);
		try(Reader inReader = new FileReader(todoFile)) {
			BufferedReader in = new BufferedReader(inReader);
			
			String line = in.readLine();
			while (line != null) {
				ToDo t = readToDo(line);
				todos.add(t);
				line = in.readLine();
			}
		}catch (Exception e) {
			System.out.println("todos.txt not existing");
		}
		
	}
	private ToDo readToDo(String line) {
		String[] attributes = line.split(SEPARATOR);
		String taskTitle = attributes[0];
		Priority prio = Priority.valueOf(attributes[1]);
		String description = attributes[2];
		String userName = attributes[3];
		
		ToDo t = new ToDo(taskTitle, prio, description, userName);
		return t;
	}
	public void loadAccounts() {
		File accountsFile = new File(ACCOUNTS_FILE);
		try(Reader inReader = new FileReader(accountsFile)) {
			BufferedReader in = new BufferedReader(inReader);
			
			String line = in.readLine();
			while(line != null) {
				Account a = readAccount(line);
				accounts.add(a);
				line = in.readLine();
			}
		}catch (Exception e) {
			System.out.println("accounts.txt not existing");
		}
		
	}
	private Account readAccount(String line) {
		String[] attributes = line.split(SEPARATOR);
		String userName = attributes[0];
		String password = attributes[1];
		Account a = new Account(userName, password);
		return a;
	}
	

	
		
	
	
	
}
	



