package server;

import java.io.IOException;
import java.net.Socket;

import commons.ChangePasswordMsg;
import commons.ChatMsg;
import commons.CreateLoginMsg;
import commons.CreateToDoMsg;
import commons.DeleteToDoMsg;
import commons.GetToDoMsg;
import commons.JoinMsg;
import commons.ListToDosMsg;
import commons.LoginMsg;
import commons.LogoutMsg;
import commons.Message;
import commons.MessageType;
import commons.PingMsg;
import commons.ResultCreateToDoMsg;
import commons.ResultGetToDo;
import commons.ResultListToDos;
import commons.ResultLoginMsg;
import commons.ResultMsg;


public class Client {
	private Socket socket;
	private String name = "<new>";
	private Model model;
	
	private boolean userNameValidforCreateLogin, userNameValidforLogin, passwordValid,taskTitleValid, descriptionValid;

	protected Client(Model model, Socket socket) {
		this.model = model;
		this.socket = socket;
		
		//Create thread to read incoming messages
		Runnable r = new Runnable() {
			@Override
			public void run() {
				while(true) {
					Message msg = Message.receive(socket);
					Message msgOut = processMessage(msg);
					msgOut.send(socket);
				}
			}

			private Message processMessage(Message msg) {
				Message msgOut = null;
				boolean btrue = true;
				boolean bfalse = false;
				
				String message = msg.toString();
				String[] messageParts = message.split("\\|");

				switch(MessageType.getType(msg)) {
				case CreateLogin:
					if(messageParts.length == 3) {
					String userName = messageParts[1].toString();
					String password = messageParts[2].toString();
					validatePassword(password); validateUserNameforCreateLogin(userName);
						if (userNameValidforCreateLogin & passwordValid) {
							model.createAccount(userName, password);
							msgOut = new ResultMsg(btrue);
						}else {
							msgOut = new ResultMsg(bfalse);
						}
					}else {
						msgOut = new ResultMsg(bfalse);
					}	
					break;
					
				case Login:
					if(messageParts.length == 3) {
						String userName = messageParts[1].toString();
						String password = messageParts[2].toString();
						validatePassword(password); validateUserNameforLogin(userName);
							if (userNameValidforLogin & passwordValid) {
								if(model.checkLogin(userName, password)) {
									int token = model.loginAccount(userName, password);
									String tokenAsString = Integer.toString(token);
								
								msgOut = new ResultLoginMsg(btrue, tokenAsString);
								}else {
									msgOut = new ResultMsg(bfalse);
								}
							}else {
								msgOut = new ResultMsg(bfalse);
							}
					}
							break;
							
				case ChangePassword:
					if (messageParts.length == 3) {
						int token = Integer.parseInt(messageParts[1]);
						String newPassword = messageParts[2].toString();
						validatePassword(newPassword);
						if(passwordValid) {
							if(model.validateToken(token)) {
								if(model.checkIfPWisNew(token, newPassword)) {
									model.setNewPassword(token, newPassword);
									
									msgOut = new ResultMsg(btrue);
								}else {
									msgOut = new ResultMsg(bfalse);
								}
							}else {
								msgOut = new ResultMsg(bfalse);
							}
						}else {
							msgOut = new ResultMsg(bfalse);
						}
					}else {
						msgOut = new ResultMsg(bfalse);
					}
					
					break;
							
				case CreateToDo:
					if(messageParts.length == 4 || messageParts.length == 5) {
						int token = Integer.parseInt(messageParts[1]);
						String taskTitle = messageParts[2].toString();
						Priority priority = Priority.valueOf(messageParts[3]);
						String description;
						if (messageParts.length == 4) {
							description = " ";
						} else {
							description = messageParts[4].toString();
						}
						
						validateTaskTitle(taskTitle);
						validateDescription(description);
						if(taskTitleValid & descriptionValid) {
																
						//validate token
						if(model.validateToken(token)) {
							int taskID = model.createToDo(taskTitle, priority, description, token);
							
							msgOut = new ResultCreateToDoMsg(btrue, taskID);
						}
						
						} else {
							msgOut = new ResultMsg(bfalse);
						}
					} else {
						msgOut = new ResultMsg(bfalse);
						
					}
					break;
					
				case ListToDos:
					if(messageParts.length == 2) {
						int token = Integer.parseInt(messageParts[1]);
						if(model.validateToken(token)) {
							String ids = model.getToDoIDs(token);
							msgOut = new ResultListToDos(btrue, ids);
						}else {
							msgOut = new ResultMsg(bfalse);
						}
						
					} else {
						msgOut = new ResultMsg(bfalse);
					}
					break;
					
				case GetToDo:
					if(messageParts.length ==3) {
						int token = Integer.parseInt(messageParts[1]);
						int taskID = Integer.parseInt(messageParts[2]);
						if (model.validateToken(token)) {
							if (model.validateTaskID(taskID)) {
								String taskTitle = model.getToDoTitle(taskID);
								Priority priotiy = model.getToDoPriority(taskID);
								String taskDescription = model.getToToDescription(taskID);
								msgOut = new ResultGetToDo(btrue, taskID, taskTitle, priotiy, taskDescription);
								
								
							} else {
								msgOut = new ResultMsg(bfalse);
							}
						}else {
							msgOut = new ResultMsg(bfalse);
						}
					}else {
						msgOut = new ResultMsg(bfalse);
					}
					break;
					
				case DeleteToDo:
					if (messageParts.length == 3) {
						int token = Integer.parseInt(messageParts[1]);
						int taskID = Integer.parseInt(messageParts[2]);
						if (model.validateToken(token)) {
							if (model.validateTaskID(taskID)) {
								model.deleteToDo(taskID);
								msgOut = new ResultMsg(btrue);
							}else {
								msgOut = new ResultMsg(bfalse);
							}
						}else {
							msgOut = new ResultMsg(bfalse);
						}
						
							
					}else {
						msgOut = new ResultMsg(bfalse);
					}
					break;
					
				case Logout:
					if (messageParts.length == 2) {
						int token = Integer.parseInt(messageParts[1]);
						if (model.validateToken(token)) {
							model.setTokenInvalid(token);
							msgOut = new ResultMsg(btrue);
						}else {
							msgOut = new ResultMsg(bfalse);
						}
					}else {
						msgOut = new ResultMsg(bfalse);
					}
					break;
			
				case Ping:
					
					if (messageParts.length == 1) {
						msgOut = new ResultMsg(btrue);
					}else if (messageParts.length == 2) {
						int token = Integer.parseInt(messageParts[1]);
						if(model.validateToken(token)) {
							msgOut = new ResultMsg(btrue);
						}else {
							msgOut = new ResultMsg(bfalse);
						}
						
					}
		
					break;
				
				default:
					msgOut = new ResultMsg(bfalse);
					break;
				}
				return msgOut;
			}

			

			

			private void validateDescription(String description) {
				if(description.length() <=255) {
					descriptionValid = true;
				}
				
			}

			private void validateTaskTitle(String taskTitle) {
				if (taskTitle.length() >=2 && taskTitle.length() <=20) {
					taskTitleValid = true;
				}
				
			}
			private void validateUserNameforLogin(String userName) {
				boolean existing = false;
				boolean valid = false;
				//check if userName already exists
				for(Account a : model.accounts) {
					if(a.getUserName().equals(userName)) {
						existing = true;
					}
				}
				//check if email is valid
				String[] addressParts = userName.split("@");
				if(addressParts.length == 2 && !addressParts[0].isEmpty() && !addressParts[1].isEmpty()) {
					if(addressParts[1].charAt(addressParts[1].length() -1) != '.') {
						String[] domainParts = addressParts[1].split("\\.");
						if (domainParts.length >=2 ) {
							valid = true;
							for (String s : domainParts) {
								if(s.length() < 2) valid = false;
							}
						}
					}
				}
				if(existing && valid) {
					userNameValidforLogin = true;
				}
			}

			private void validatePassword(String password) {
				if(password.length() >= 3 && password.length() <=20) {
					passwordValid = true;
				}
				
			}

			private void validateUserNameforCreateLogin(String userName) {
				boolean existing = true;
				boolean valid = false;
				//check if userName already exists
				for(Account a : model.accounts) {
					if(a.getUserName().equals(userName)) {
						existing = false;
						
					}
				}
				
				//check if email is valid
				String[] addressParts = userName.split("@");
				if(addressParts.length == 2 && !addressParts[0].isEmpty() && !addressParts[1].isEmpty()) {
					if(addressParts[1].charAt(addressParts[1].length() -1) != '.') {
						String[] domainParts = addressParts[1].split("\\.");
						if (domainParts.length >=2 ) {
							valid = true;
							for (String s : domainParts) {
								if(s.length() < 2) valid = false;
							}
						}
					}
				}
				if(existing && valid) {
					userNameValidforCreateLogin = true;
				}
			}
		};
		Thread t = new Thread(r);
		t.start();
	}
	protected void sendResult(ResultMsg msg) {
		msg.send(socket);
		
	}
	//TODO for other MessageTypes?
	public void send(ChatMsg msg) {
		msg.send(socket);
	}
	
	public void send(CreateLoginMsg msg) {
		msg.send(socket);
	}
	
	public void send(LoginMsg msg) {
		msg.send(socket);
	}
	public void send(LogoutMsg msg) {
		msg.send(socket);
		
	}
	public void send(CreateToDoMsg msg) {
		msg.send(socket);
		
	}
	public void send(ChangePasswordMsg msg) {
		msg.send(socket);
		
	}
	public void send(GetToDoMsg msg) {
		msg.send(socket);
		
	}
	public void send(DeleteToDoMsg msg) {
		msg.send(socket);
	}
	
	public void send(ListToDosMsg msg) {
		msg.send(socket);
	}
	
	public void send(PingMsg msg) {
		msg.send(socket);
		
	}
	public void send(ResultMsg msg) {
		msg.send(socket);
		
	}

	public void stop() {
		try {
			socket.close();
		} catch (IOException e) {
		}
	}
		
		@Override
		public String toString() {
			return name + ": " + socket.toString();
		}
		


}
