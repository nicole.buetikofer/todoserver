package server;

public class ToDo {
	private String taskTitle;
	
	private static int taskIDCount = 0;
	private int taskID;
	private Priority priority;
	private String taskDescription;
	String userName;
	
	public ToDo (String taskTitle, Priority priority, String taskDescription, String userName) {
		this.taskTitle = taskTitle;
		this.priority = priority;
		this.taskDescription = taskDescription;
		taskIDCount++;
		this.taskID = taskIDCount;
		this.userName = userName;
		// userName als Zuordnung zu welchem User der Task gehört
	}
	
	public ToDo(String taskTitle, Priority priority, String taskDescription, String userName, int taskID) {
		this.taskTitle = taskTitle;
		this.priority = priority;
		this.taskDescription = taskDescription;
		this.taskID = taskID;
	}
	
	public String getTaskTitle() {
		return taskTitle;
	}
	
	public int getTaskID() {
		return this.taskID;
	}
	
	public String getUserName() {
		return userName;
	}
	
	public void setTaskTitle(String taskTitle) {
		if (taskTitle.length() >=3 && taskTitle.length() <= 20) {
		this.taskTitle = taskTitle;
		}
	}
	
	public Priority getPriority() {
		return priority;
	}
		
	public void setPriority(Priority priority) {
			this.priority = priority;
	}
	
	public String getTaskDescription() {
		return taskDescription;
	}
	
	public void setTaskDescription(String taskDescription) {
		if (taskDescription.length() >= 0 && taskDescription.length() <= 255 ) {
		this.taskDescription = taskDescription;
		}
	}
	
	public boolean equals(ToDo t) {
		return (this.taskTitle == t.taskTitle);
	}
	


}
