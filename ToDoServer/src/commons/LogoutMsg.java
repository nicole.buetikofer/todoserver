package commons;

public class LogoutMsg extends Message{
	//Never fails, token becomes invalid

	private String token;

	public LogoutMsg(String token) {
		super(MessageType.Logout);
		this.token = token;
		
	}
	

	public String toString() {
		return "Logout" + '|' + token;
	}
	

}
