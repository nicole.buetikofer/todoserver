package commons;

public class PingMsg extends Message{
	private String token;
	
	public PingMsg(String token) {
		super(MessageType.Ping);
		this.token = token;
	}
	
	public PingMsg() {
		super(MessageType.Ping);
	}
	
	public String toString() {
		String s;
		if (token == null) {
			s = type.toString() ;
		}else {
			s = type.toString() + "|" + token;
		}
		return s;
		
	}

}
