package commons;

public class ResultCreateToDoMsg extends Message{
	private boolean result;
	private int taskID;
	
	public ResultCreateToDoMsg(boolean result, int taskID) {
		super(MessageType.Result);
		this.result = result;
		this.taskID = taskID;
	}
	
	public String toString() {
		return type.toString() + "|" + result +"|"+ taskID;
	}

}
