package commons;

public class ResultLoginMsg extends Message{
	private boolean result;
	private String token;
	
	public ResultLoginMsg(boolean result, String token) {
		super(MessageType.ResultLogin);
		this.result = result;
		this.token = token;
	}
	
	public String toString() {
		return "Result" + "|" + result + "|" + token;
	}

}
