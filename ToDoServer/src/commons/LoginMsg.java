package commons;

public class LoginMsg extends Message{
	private String name;
	private String password;
	
	public LoginMsg(String name, String password) {
		super(MessageType.Login);
		this.name = name;
		this.password = password;
	}
	
	public String getName() {
		return name;
	}
	
	public String getPassword() {
		return password;
	}
	
	@Override
	public String toString() {
		return type.toString() + '|' + name + '|' + password;
	}

}
