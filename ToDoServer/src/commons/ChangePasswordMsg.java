package commons;

import java.util.Random;

public class ChangePasswordMsg extends Message{
	private String password;
	private String token;
	Random rand = new Random();
		
	public ChangePasswordMsg(String token, String password) {
		super(MessageType.ChangePassword);
		this.password = password;
		this.token = token;
		
	}
	
	public void setPassword(String password) {
		this.password = password;
	}
	
	public String toString() {
		return type.toString() + '|' + token + '|' + password;
	}

	
	
	

}
