package commons;

public class ListToDosMsg extends Message{
	private String token;
	
	public ListToDosMsg(String token) {
		super(MessageType.ListToDos);
		this.token = token;
		
	}
	public String toString() {
		return type.toString() + "|" + token;
	}
	

}
