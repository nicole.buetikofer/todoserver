package commons;

public class ResultListToDos extends Message{
	private boolean result;
	private String ids;
	
	public ResultListToDos(boolean result, String ids) {
		super(MessageType.Result);
		this.result = result;
		this.ids = ids;;
	}
	
	public String toString() {
		return type.toString() + "|" + result  + ids;
	}
	

}
