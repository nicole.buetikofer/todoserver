package commons;

public class DeleteToDoMsg extends Message{
	private int ID;
	private String token;
	
	public DeleteToDoMsg(String token, int ID) {
		super(MessageType.DeleteToDo);
		this.ID = ID;
		this.token = token;
	}
	
	public String toString() {
		return type.toString() + "|" + token + "|" + ID;
	}

}
