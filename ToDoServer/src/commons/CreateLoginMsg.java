package commons;

public class CreateLoginMsg extends Message{
	private String email;
	private String password;
	
	
	public CreateLoginMsg(String email, String password) {
		super(MessageType.CreateLogin);
		this.email = email;
		this.password = password;
	}
	
	public String getEmail() {
		return email;
	}
	
	public String getPassword() {
		return password;
	}
	
	public String toString() {
		return type.toString() + '|' + email + '|' + password;
	}
	
	
	
}
