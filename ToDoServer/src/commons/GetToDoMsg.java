package commons;

public class GetToDoMsg extends Message{
	private int ID;
	private String token;

	public GetToDoMsg(String token, int ID) {
		super(MessageType.GetToDo);
		this.token = token;
		this.ID = ID;

	}
	
	public String toString() {
		return type.toString() + '|' + token + "|" + ID;
	}

}
