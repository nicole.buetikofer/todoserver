package commons;


import server.Priority;
import server.ToDo;

public class CreateToDoMsg extends Message{
	String task, description;
	//int token;
	String token;
	Priority priority;

	public CreateToDoMsg(String token, String task, Priority priority, String description) {
		super(MessageType.CreateToDo);
		this.task = task;
		this.token = token;
		this.description = description;
		this.priority = priority;
		
	}
	
//	public CreateToDoMsg(String token, String task, Priority priority) {
//		super(MessageType.CreateToDo);
//		this.task = task;
//		this.token = token;
//		this.priority = priority;
//	}

	public String toString() {
//		String s;
//		if(description.equals("")) {
//			s = type.toString() + '|' + token + '|' + task + '|' + priority;
//		}else {
			return  type.toString() + '|' + token + '|' + task + '|' + priority +'|' + description;
//		}
//		return s;
	}
	

}
