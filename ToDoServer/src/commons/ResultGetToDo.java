package commons;

import server.Priority;

public class ResultGetToDo extends Message{
	private boolean result;
	private int taskID;
	private String task;
	private Priority priority;
	private String description;
	
	public ResultGetToDo(boolean result, int taskID, String task, Priority priority, String description) {
		super(MessageType.Result);
		this.result = result;
		this.taskID = taskID;
		this.task = task;
		this.priority = priority;
		this.description = description;
		
	}
	
	public String toString() {
		return type.toString() + "|" + result + "|" + taskID + "|" + task + "|" + priority.toString() + "|" + description;
	}

}
