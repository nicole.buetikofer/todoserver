package commons;

public enum MessageType {
	Join, Chat, CreateLogin, Login, ChangePassword, Logout, CreateToDo, GetToDo, DeleteToDo, ListToDos, Ping, Result, ResultLogin;

	
	public static MessageType getType(Message msg) {
		MessageType type = MessageType.Ping;
		if(msg instanceof CreateLoginMsg) type = CreateLogin;
		else if (msg instanceof LoginMsg) type = Login;
		else if (msg instanceof ChangePasswordMsg ) type = ChangePassword;
		else if (msg instanceof LogoutMsg ) type = Logout;
		else if (msg instanceof CreateToDoMsg ) type = CreateToDo;
		else if (msg instanceof GetToDoMsg ) type = GetToDo;
		else if (msg instanceof DeleteToDoMsg ) type = DeleteToDo;
		else if (msg instanceof ListToDosMsg ) type = ListToDos;
		else if (msg instanceof ResultMsg ) type = Result;
		else if (msg instanceof ResultLoginMsg) type = ResultLogin;
		
		return type;
	}
	
	

}
