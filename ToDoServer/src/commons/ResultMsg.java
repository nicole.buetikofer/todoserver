package commons;

public class ResultMsg extends Message {
	private boolean result;
	private int tokenOrID;
	private int taskID;
	private int s;
	
	//Result|true/false
	public ResultMsg(boolean result) {
		super(MessageType.Result);
		this.result = result;
		s =1;
	}
	
	public ResultMsg(int tokenOrID) {
		super(MessageType.Result);
		this.tokenOrID = tokenOrID;
		this.result = true;
		s =2;
	}
	

	public String toString() {	
		return type.toString() + "|" + result;
		
			//TODO true if the command succeeded, no data to return
			//true if the command succeeded, and returned data
			//A successful login returns the token as its data
			//false if a command fails for any reason
		
	}

}
