package commons;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.Socket;
import java.util.ArrayList;
import java.util.logging.Logger;

import server.Priority;


public abstract class Message {
	
	private static Logger logger = Logger.getLogger("");
	protected MessageType type;
	
	public Message(MessageType type) {
		this.type = type;
		
	}
	
	public void send(Socket socket) {
		OutputStreamWriter out;
		try {
			out = new OutputStreamWriter(socket.getOutputStream());
			logger.info("Sending message: " + this.toString());
			out.write(this.toString() + "\n");
			out.flush();
		}catch (IOException e) {
			logger.warning(e.toString());
		}
		
	}
	
	public static Message receive(Socket socket) {
		BufferedReader in;
		Message msg = null;
		try {
			in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
			String msgText = in.readLine();
			logger.info("Receiving message: " + msgText);
			
			//Parse message
			String[] parts = msgText.split("\\|");
//			if (parts[0].equals(MessageType.Join.toString())) {
//				msg = new JoinMsg(parts[1]);
//				
//			}if (parts[0].equals(MessageType.Chat.toString())) {
//				msg = new ChatMsg(parts[1], parts[2]);
				//}
		if (parts[0].equals(MessageType.CreateLogin.toString())) {
				msg = new CreateLoginMsg(parts[1], parts[2]);
				
			}if (parts[0].equals(MessageType.Login.toString())) {
				msg = new LoginMsg(parts[1], parts[2]);
				
			}if (parts[0].equals(MessageType.Logout.toString())) {
				msg = new LogoutMsg(parts[1]);
				
			}if(parts[0].equals(MessageType.CreateToDo.toString())) {
				String token =parts[1].toString();
				//token = Integer.parseInt(parts[1]);
				Priority priority = Priority.valueOf(parts[3]);
//				if(parts.length == 4) {
//					msg = new CreateToDoMsg(token, parts[2], priority);
//				}else {
					msg = new CreateToDoMsg(token, parts[2], priority, parts[4]);
//				}
				
				
			}if(parts[0].equals(MessageType.ChangePassword.toString())) {
				//int token = Integer.parseInt(parts[1]);
				String token = parts[1].toString();
				msg = new ChangePasswordMsg(token, parts[2]);
			}
			
			if(parts[0].equals(MessageType.GetToDo.toString())) {
				//int token = Integer.parseInt(parts[1]);
				String token = parts[1].toString();
				int id = Integer.parseInt(parts[2]);
				msg = new GetToDoMsg(token, id);
			}
			if (parts[0].equals(MessageType.DeleteToDo.toString())) {
				//int token = Integer.parseInt(parts[1]);
				String token = parts[1].toString();
				int id = Integer.parseInt(parts[2]);
				msg = new DeleteToDoMsg(token, id);
			}
			if (parts[0].equals(MessageType.ListToDos.toString())) {
				String token = parts[1].toString();
				//int token = Integer.parseInt(parts[1]);
				msg = new ListToDosMsg(token);
			}
			if(parts[0].equals(MessageType.Ping.toString())) {
				if(parts.length == 1) {
					msg = new PingMsg();
				}else {
				//int token = Integer.parseInt(parts[1]);
				String token = parts[1].toString();
				msg = new PingMsg(token);
				}
			}
			if (parts[0].equals(MessageType.Result.toString())) {
				String result = parts[1].toString();
				boolean b = Boolean.parseBoolean(result);
				if(parts.length == 2) {
					msg = new ResultMsg(b);
				} else if (parts.length == 3) {
					String token = parts[2].toString();
//					if(parts[2].toString().matches("[+-]?\\d*(\\.\\d+)?")) {
//						
//					}
//						
//						int tokenorid = Integer.parseInt(parts[2]);
						msg = new ResultLoginMsg(b, token);
					
				} else if (parts.length == 6) {
					if(parts[5].toString().matches("[+-]?\\d*(\\.\\d+)?")) {
						String ids= "";
						String s1 = parts[2].toString();
						String s2 = parts[3].toString();
						String s3 = parts[4].toString();
						String s4 = parts[5].toString();
						ids += "|" + s1 + "|" + s2 + "|" + s3 +"|" + s4;
						msg = new ResultListToDos(b, ids);
					}else {
					int id = Integer.parseInt(parts[2]);
					String task = parts[3].toString();
					Priority priority = Priority.valueOf(parts[4]);
					String description = parts[5].toString();
					
					if(description.equals(null)) {
						description = " ";
					}
				
					msg = new ResultGetToDo(b, id, task, priority, description);
					}
				}else {
					ArrayList<String> iDs = new ArrayList<String>();
					String ids= "";
					int l = parts.length-1;
					for (int i = 2; i <= l; i++) {
						String s = parts[i].toString();
						iDs.add(s);
					}
					for(String s : iDs) {
						ids += "|" +s;
					}
					msg = new ResultListToDos(b, ids);
				}
				
		}
				
//				if(msg.getType().equals(MessageType.ResultLogin)) {
//					int token = Integer.parseInt(parts[1]);
//					msg = new ResultLoginMsg(b, token);
//				}else {
//					
//				}
				
		
			
				
			
		}catch (IOException e) {
			logger.warning(e.toString());
		}
		return msg;
	}
	
	public MessageType getType() {
		return type;
	}

	

}
