# ToDoServer

README
Nicole Bütikofer
Sarah Büttler
Implemented features:
	- Java server
	- Synchronous messaging with plain text
	- List, Create, Get and Delete ToDo entries 
	- Create Login, Login, Logout, Change Password
	- Ping
	- Results
	- Data is validated on the server
	- Real tokens (big number between 1 and 1000000)
	- Save data when Server GUI closes
	- Read data when server GUI starts
	
	- Client
	- Input data is validated on the client
    - Messages are shown on client GUI

	- css styling for client and server

